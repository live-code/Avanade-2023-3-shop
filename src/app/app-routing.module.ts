import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: 'shop', loadChildren: () => import('./features/shop/shop.module').then(m => m.ShopModule) }, { path: 'cart', loadChildren: () => import('./features/cart/cart.module').then(m => m.CartModule) }, { path: 'checkout', loadChildren: () => import('./features/checkout/checkout.module').then(m => m.CheckoutModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
