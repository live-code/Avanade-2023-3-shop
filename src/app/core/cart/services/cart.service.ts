import { Injectable } from '@angular/core';
import { CartItem } from '../../../model/cart-item';
import { Product } from '../../../model/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: CartItem[] = [];

  addToCart(p: Product) {
    this.openCartPanel();
    const found = this.items.find(item => item.product.id === p.id)
    if (!found) {
      // add product
      const cartItem: CartItem = { product: p, qty: 1 };
      this.items = [...this.items, cartItem]
    } else {
      this.incrementQty(p)
    }


  }

  removeFromCart(product: Product) {
    this.items = this.items.filter(
      item => item.product.id !== product.id
    )
  }


  incrementQty(product: Product) {
    const found = this.items.find(item => item.product.id === product.id);
    if (found) {
      const cartItem: CartItem = {...found, qty: found.qty + 1}
      this.items = this.items.map(item => {
        return item.product.id === cartItem.product.id ? cartItem : item;
      })
    }
  }

  decrementQty(product: Product) {
    const found = this.items.find(item => item.product.id === product.id);

    if (found && found.qty === 1) {
      this.removeFromCart(product)
    }


    if (found && found.qty > 1) {
      const cartItem: CartItem = {...found, qty: found.qty - 1}
      this.items = this.items.map(item => {
        return item.product.id === cartItem.product.id ? cartItem : item;
      })
    }
  }


  get totalItems() {
    return this.items.length
  }


  // PANEL OVERLAY MANAGEMENT
  isCartPanelOpened = false;

  closeCartPanel() {
    this.isCartPanelOpened = false;
  }
  openCartPanel() {
    this.isCartPanelOpened = true;
  }
  toggle() {
    this.isCartPanelOpened = !this.isCartPanelOpened;
  }
}
