import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-cart-panel',
  template: `
    <div class="cart-preview"
         *ngIf="cartService.isCartPanelOpened">
      
      <div *ngIf="cartService.items.length === 0">
        empty cart
      </div>
      
      <div *ngFor="let item of cartService.items">

        <div class="d-flex justify-content-between">
          <div>{{item.product.name}}</div>
          
          <div>
            <i class="fa fa-minus-circle" (click)="cartService.decrementQty(item.product)"></i>
              {{item.qty}}
            <i class="fa fa-plus-circle" (click)="cartService.incrementQty(item.product)"></i>
            
          </div>

          <div class="d-flex gap-2 align-items-center">
            <div>€{{item.product.cost}}</div>
            <i
              class="fa fa-trash"
              (click)="cartService.removeFromCart(item.product)"
            ></i>
          </div>
        </div>
      </div>


      <div class="btn btn-primary" *ngIf="cartService.items.length" (click)="gotoCheckout()">
        Go to checkout
      </div>
    </div>
    
  `,
  styles: [`
    .cart-preview {
      background-color: gray;
      color: white;
      position: fixed;
      top: 60px;
      right: 20px;
      width: 270px;
      border-radius: 20px;
      padding: 20px;
      z-index: 10;
    }
  
  `]
})
export class CartPanelComponent {

  constructor(
    public cartService: CartService,
    private router: Router
  ) {
  }

  gotoCheckout() {
    this.cartService.closeCartPanel()
    this.router.navigateByUrl('checkout')
  }
}
