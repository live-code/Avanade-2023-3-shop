import { Pipe, PipeTransform } from '@angular/core';
import { CartItem } from '../../../model/cart-item';
import { Product } from '../../../model/product';

@Pipe({
  name: 'getCartItems'
})
export class GetCartItemsPipe implements PipeTransform {

  transform(items: CartItem[]):string {
    if (items.length === 0) {
      return 'no items'
    }

    if (items.length === 1) {
      return '1 item'
    }

    return `${items.length} items`
  }

}
