import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { GetCartItemsPipe } from './cart/pipes/get-cart-items.pipe';
import { NavbarComponent } from './components/navbar.component';
import { CartPanelComponent } from './cart/components/cart-panel.component';


@NgModule({
  declarations: [
    NavbarComponent,
    GetCartItemsPipe,
    CartPanelComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
  ],
  exports: [
    NavbarComponent,
    CartPanelComponent
  ]
})
export class CoreModule { }
