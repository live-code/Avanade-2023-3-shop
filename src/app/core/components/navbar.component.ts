import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CartService } from '../cart/services/cart.service';

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar navbar-expand bg-body-tertiary">
      <div class="container-fluid">
        <a class="navbar-brand">Logo</a>
    
          <div class="d-flex justify-content-between align-items-center">
            <ul class="navbar-nav">
              <li class="nav-item" routerLink="shop">
                <a class="nav-link"  routerLinkActive="active">shop</a>
              </li>
              <li class="nav-item" routerLink="cart">
                <a class="nav-link"  routerLinkActive="active" >cart</a>
              </li>
            </ul>
            
            <div>
              <div 
                class="badge rounded-pill text-bg-primary"
                (click)="cartService.toggle()"
              >
                {{cartService.items | getCartItems }}
              </div>
            </div>
          </div>
      </div>
    </nav>

    
  `,

})
export class NavbarComponent {

  constructor(
    public cartService: CartService
  ) {
  }
}
