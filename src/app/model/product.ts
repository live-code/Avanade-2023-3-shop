export interface Product {
  id: number;
  desc: string;
  name: string;
  cost: number;
}
