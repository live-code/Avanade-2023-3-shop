import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
   <app-navbar></app-navbar>
   <app-cart-panel></app-cart-panel>
   
   <div class="container">
    <router-outlet></router-outlet>
   </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'avanade-23-3-shop';
}
