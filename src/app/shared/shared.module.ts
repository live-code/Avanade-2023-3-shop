import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetCartItemsPipe } from '../core/cart/pipes/get-cart-items.pipe';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule
  ],
  exports: [
  ]
})
export class SharedModule { }
