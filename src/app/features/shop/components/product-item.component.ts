import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'app-product-item',
  template: `
    <div class="card">
      <div class="card-header">
        {{product?.name}}
      </div>
      <div class="card-body">
        {{product?.desc}}

        <button 
          class="btn btn-primary" 
          (click)="addToCart.emit(product)"
        ><i class="fa fa-cart-plus"></i></button>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class ProductItemComponent {
  @Input() product: Product | undefined;
  @Output() addToCart = new EventEmitter<Product>()
}
