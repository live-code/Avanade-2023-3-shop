import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, delay, distinctUntilChanged, filter, map, mergeAll, mergeMap } from 'rxjs';
import { CartService } from '../../core/cart/services/cart.service';
import { Product } from '../../model/product';

@Component({
  selector: 'app-shop',
  template: `
    
    <input type="text" class="form-control" placeholder="search" [formControl]="search">
    <br>
    
    <div class="d-flex gap-3">
      <app-product-item
        *ngFor="let p of products"
        [product]="p"
        (addToCart)="addToCart($event)"
      ></app-product-item>
    </div>
  `,
  styles: [
  ]
})
export class ShopComponent {
  products: Product[] = [];
  search = new FormControl();

  constructor(
    private http: HttpClient,
    private cartService: CartService
  ) {
    this.getAllProducts();
    this.searchInit()
  }

  searchInit() {
    this.search.valueChanges
      .pipe(
        filter(text => text.length > 2),
        debounceTime(1000),
        distinctUntilChanged(),
        mergeMap(text => this.http.get<Product[]>(`http://localhost:3000/products?q=${text}`)),
      )
      .subscribe(res => {
        this.products = res;
      })
  }

  getAllProducts() {
    this.http.get<Product[]>('http://localhost:3000/products')

      .subscribe(res => {
        this.products = res;
      })
  }

  addToCart(p: Product) {
    this.cartService.addToCart(p);
  }
}
