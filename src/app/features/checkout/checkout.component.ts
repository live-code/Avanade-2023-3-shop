import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-checkout',
  template: `
    <h1>Checkout</h1>
 <!--   
    <div *ngIf="inputName.errors?.['required']">required field</div>
    <div *ngIf="inputName.errors?.['minlength']">min 3 chars</div>
    <input 
      type="text" [formControl]="inputName"
      class="form-control"
      [ngClass]="{'is-invalid': inputName.invalid && inputName.dirty}"
    >
    -->
    
    <form [formGroup]="form" (submit)="save()">
      <input type="text" formControlName="name">
      <input type="text" formControlName="surname">
      <button (click)="clear()" type="button">CLEAR</button>
      <button [disabled]="form.invalid" type="submit">SAVE</button>
    </form>
    
    <div>Value: {{form.value | json}}</div>
    <div>valid: {{form.valid}}</div>
    
  `,
})
export class CheckoutComponent {
  form = new FormGroup({
    name: new FormControl('', { nonNullable: true, validators: [Validators.required, Validators.minLength(3)]}, ),
    surname: new FormControl('', { nonNullable: true}),
  })

  save() {
    console.log(this.form.value)
  }

  clear() {
    this.form.reset();
  }
}
